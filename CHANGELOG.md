## [1.2.2](https://gitlab.com/to-be-continuous/renovate/compare/1.2.1...1.2.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([e32e815](https://gitlab.com/to-be-continuous/renovate/commit/e32e815f6aa487edf0c25112cc6173bbd1ce0ad2))

## [1.2.1](https://gitlab.com/to-be-continuous/renovate/compare/1.2.0...1.2.1) (2024-2-5)


### Bug Fixes

* set platform to gitlab ([df1894c](https://gitlab.com/to-be-continuous/renovate/commit/df1894cb110a180bd871185fa3585b78588b23ba))

# [1.2.0](https://gitlab.com/to-be-continuous/renovate/compare/1.1.0...1.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([0ee4e8d](https://gitlab.com/to-be-continuous/renovate/commit/0ee4e8d8dd36a73e8d41e33fad7241678a652d1a))

# [1.1.0](https://gitlab.com/to-be-continuous/renovate/compare/1.0.0...1.1.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([fbee9a0](https://gitlab.com/to-be-continuous/renovate/commit/fbee9a02c20ac092cb10faf39582945bf219b54a))

# 1.0.0 (2023-11-25)


### Features

* initial template ([b3b3d27](https://gitlab.com/to-be-continuous/renovate/commit/b3b3d27edd00525afe57627a871db661f2201f9c))
