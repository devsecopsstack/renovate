# GitLab CI template for Renovate

Automate your dependency updates with [Renovate](https://www.mend.io/renovate/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # include the component
  - component: gitlab.com/to-be-continuous/renovate/gitlab-ci-renovate@1.2.2
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # include the template
  - project: 'to-be-continuous/renovate'
    ref: '1.2.2'
    file: '/templates/gitlab-ci-renovate.yml'
```

## Configuration

The Renovate template uses some global configuration used throughout all jobs.

| Input / Variable | Description                                                                     | Default value     |
|------------------------|---------------------------------------------------------------------------------|-------------------|
| `image` / `RENOVATE_IMAGE` | The Docker image used to run Renovate                                           | `registry.hub.docker.com/renovate/renovate:latest` |
| `platform` / `RENOVATE_PLATFORM` | The Platform type of repository [See doc](https://docs.renovatebot.com/self-hosted-configuration/#platform)  | `gitlab` |
| :lock: `RENOVATE_TOKEN`| A GitLab access token to allow Renovate crawl your projects. [See doc](https://docs.renovatebot.com/modules/platform/gitlab/#authentication) | _none_ |
| :lock: `GITHUB_COM_TOKEN`| A GitHub access token to allow Renovate fetch changelogs. [See doc](https://docs.renovatebot.com/getting-started/running/#githubcom-token-for-changelogs) | _none_ |

This template will help you using [Renovate](https://www.mend.io/renovate/) from a GitLab project to
automate your dependency updates within your groups or projects.
On the contrary to other to-be-continuous templates, this one should be used in a separate project that
will be in charge of crawling all your other projects.

Upon including the template, carefuly follow [Renovate's documentation](https://docs.renovatebot.com/) to
configure the bot accordingly. Pay attention to the following:

* ~~Remember to set the [platform](https://docs.renovatebot.com/self-hosted-configuration/#platform) parameter
  to `gitlab` in your configuration.~~
* [GitLab platform integration](https://docs.renovatebot.com/modules/platform/gitlab/) requires that you
  declare a `RENOVATE_TOKEN` variable with an access token.
* You'll also probaly need to declare a `GITHUB_COM_TOKEN` variable, holding a GitHub access token
  (for [fetching changelogs](https://docs.renovatebot.com/getting-started/running/#githubcom-token-for-changelogs))

### Dry-run implementation details

Depending on the source of a pipeline, the template will either perform your dependency updates (create/update/delete branches and MRs)
or [perform a dry run](https://docs.renovatebot.com/self-hosted-configuration/#dryrun) (to preview the behavior of Renovate with logs, without making any changes to your repositories).

The real dependency updates is triggered only when:

- the pipeline is started from the **web interface** (menu _Build > Pipelines_, click _Run pipeline_),
- or the pipeline is started from a **scheduled run** (menu _Build > Pipeline schedules_).

In any other case (regular Git commit or else) the template will perform a dry run.

:warning: Dry-run behavior is enabled/disabled with the `RENOVATE_DRY_RUN` environment variable. 
Please don't try to override it or you might break the default template implementation.
